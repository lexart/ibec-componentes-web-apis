Base de datos NoSQL MongoDB

	- Studio 3T
	- Robo 3T
		~> Gestores de base de datos
		~> Adm. colecciones



	¿Que es una colección? 

		Base de datos NoSQL es una base de documentos
			~> Documentos: JSON
			A los documentos JSON de esa base, se llaman colecciones

		Fundamentos de datos:

			~> Obtener información (datos)
			~> Insertar información
			~> Actualizar la información
			~> Eliminar información

		Si cumple estos cuatro fundamentos, nuestra base de datos es usable
		en un sistema.

		Las colecciones tienen una estructura que soporta "ese" modelo llamado "schema"

			"schema" es el esqueleto de un modelo en un documento JSON

		Ejemplo: 

			Colección: personas.json
			[
				{
					"nombre":"Alex",
					"edad":28,
					"cedula":123123
				}
			]

			Schema sería "persona":

			{
				"nombre":"string",
				"edad":number,
				"cedula":number
			}

			~

			{
				"nombre": type: "string",
				"edad": type: number,
				"cedula":type: number
			}

		Usando [Robo 3T]

			1. Click derecho sobre "local" > Create database
				~> Una vez que creas la base de datos
				~> Observamos 3 carpetas:
					Collections/
					Functions/
					Users/
			2. Arriba de la carpeta Collections, click derecho "create collection"
				~> Le agregamos el nombre de la colección

			3. Crear por lo menos algunos datos de ejemplo: 
				~> Crear implica: Fundamento 2, insertar información
				~> Click derecho sobre la colección "Insert document"

				~> Crear el objeto JSON con la persona: 

					[Objeto ~ Documento]

					{
						"nombre":"Alex",
						"edad":28,
						"documento":123123
					}
					~
					{
						"_id" ObjectId
						"nombre" String,
						"edad" Int32,
						"documento" Int32
					}

					ObjectId: Es el identificador único dentro de los documentos de la colección

			Obs: Tener cuidado con los documentos sobre una colección ya que los schemas son ampliables y no cuida la consistencia del número de propiedades en un documento.


			[Actualizar la información]: 

				1. Click derecho sobre un documento luego: "Edit document"
				2. Luego click en "Save"

			Obs: Los documentos de una colección son tan flexibles que puede haber errores sobre propiedades que a lo largo de los documentos tienen el mismo significado

			Ejemplo: Edito una colección que no tenía teléfono y le agrego el campo: "telefono", sin embargo los demás documentos tenían el campo "tel" para el mismo propósito

			Mas adelante en una API si tengo definido el schema: 
				
				{
					"nombre" String, 
					"edad" Int32
				}
			
			No me puede llegar un documento del siguiente tipo: 

				{
					"nombre" String,
					"edad" Int32,
					"documento" Int32
				}

			Tengo dos opciones: 
				1. Error en de actualización/insert
				2. Actualizo o inserto solo las propiedades válidas


			[Eliminar documento]

				1. Click derecho sobre el documento, luego: "Delete document..."
				2. Despliega un alerta, luego elimina

			[Listado de la información]

				Query's de MongoDB

					- ¿Cómo listo todos los documentos?

					Collection.find({})

						~> llama una función: "find(...)" y esa función tiene como 
						parametro de entrada un objeto: "{}"

						"{}" ~> Un objeto vacío o todas las propiedades

					Todo objeto por mas chico que sea tiene al objeto vacío incluido

						Ejemplo: A: {nombre: "Alex"}

						Espacio de A: [{},{nombre:"Alex"}]


					- ¿Cómo haría para obtener todos los documentos que tienen el nombre: "Alex"?

					Collection (personas)

						A: {nombre:"Alex", ...}
						B: {nombre:"Luis", ...}
						C: {nombre:"Alex"}

						P(A): [{},{nombre:"Alex"},{nombre:"Alex", ...},{...}]
						P(B): [{},{nombre:"Luis"},{nombre:"Luis", ...},{...}]
						P(C): [{},{nombre:"Alex"}]

					- Comienzo a dejar más especifica la búsqueda: 

						{nombre: "Alex", edad: 28}

						[Búsqueda]: Buscá un objeto con nombre:"Alex" Y edad:28 dentro de los documentos de la colección

					- Quiero una búsqueda especifica y única

						~ Solo un documento va aparecer como resultado de la búsqueda
						y no más.

						~> Hago una búsuqeda por su ObjectId

						Collection.find({_id:ObjectId("5f0654286fa4e48744a64013")})

					- Quiero buscar sobre un rango de edad

						* Ejemplo: Todos los documentos donde la edad es menor a 30 años

							Utilizamos el literal: $lt (less than)

							Y utilizamos la búsqueda de la siguiente manera:

								.find({ edad: { $lt: 30 } })

					- Query con condicional OR

						* Ejemplo: Obtener todos los documentos que tienen nombre: "Alex" OR edad: 19

						Utilizar el literal: {$or: [...]}

						.find({$or: [{nombre:"Alex"}, {edad:19}]})

					- Verificación de estados


						Ejemplo: En un sistema quiero obtener los usuarios que son de tipo: Admin y Editor. Tomando en cuenta que en el sistema existe 4 roles: 
							
							- Admin 	 (A)
							- Editor	 (E)
							- Invitado 	 (I)
							- Supervisor (S)

							* Crear la colección "usuarios"

							Schema: 
								
								{
									"nombre" String,
									"usuario" String, 
									"clave" String,
									"tipo" String
								}

						¿Cómo sería la query?

						TIPO OR: 
							{$or: [{tipo:"A"}, {tipo:"E"}]} √

						TIPO IN: 
							{tipo: {$in: ["A","E"]}} √

					- Objetos con profundidad

						Colección: usuarios

							Documento: 

								{
									"nombre":"Alex",
									"usuario":"lexcasa",
									"tipo": "A"
								}

							No se puede determinar que significa: "A"

							Doc. Completo:

								{
									"nombre":"Alex",
									"usuario":"lexcasa",
									"tipo": {
										"rol":"Admin",
										"id":"A"
									}
								}

							En este caso puedo determinar que significa: "A", que es la "id" asociada a un rol de usuario

							Si yo quiero acceder a una propiedad del documento que está dentro de otro objeto accedo de la siguiente manera: 

								.find({"tipo.id":"A"})

								"tipo.id" ~ ..."tipo": {"id": "A"}

							¿Cómo obtendría todos los documentos de tipo/rol admin?

								.find({"tipo.id":"A"})

								{$or: [{"tipo.id":"A"}, {tipo:"A"}]} √

							* Si quiero traer tipo/rol admin y editor

								{$or: [{"tipo.id":"A"}, {tipo:"A"}, {tipo:"E"}]}

						Documentos más profundos:

						CASO 1:

							{
								"nombre":"Alex",
								"tipo":{
									"rol":{
										"nombre": "Admin",
										"id":"A"
									},
									"nombre":"cliente"
								},
								"documento":123123
							}
					    
					    CASO 2:

					    	{
								"nombre":"Juan",
								"tipo":{
									"rol":{
										"nombre": "Editor",
										"id":"E"
									},
									"nombre":"cliente"
								},
								"documento":{
									"tipo":"RUT",
									"numero":2222222
								}
							}

						Ejemplo: 
							1. Obtener todos los documentos que son empresas de tipo admin en el sistema.

							2. Obtener todos los documentos que son empresas de tipo editor en el sistema.

							~> Como identifico una "empresa" en un documento y como idn. un admin?

								Admin: 
									"tipo.rol.id": "A"

								Empresa:
									"documento.tipo": "RUT"

								CASO 1, no tengo como afirmar si es o no empresa pero si el tipo
								CASO 2, puedo afiermar si es o no empresa y el tipo

							Query (1): 
								.find({"documento.tipo": "RUT", "tipo.rol.id": "A"})

							Query (2):
								.find({"documento.tipo": "RUT", "tipo.rol.id": "E"})

							3. Obtener todos los usuarios de tipo admin

								.find({$or: [{"tipo.id":"A"}, {"tipo.rol.id":"A"}, {tipo:"A"}]})


						CASO: Un usuario puede tener mas de un permiso sobre el sistema
							
							- Un suario puede ser: 
								- Supervisor y editar items
								- Admin y supervisor
								- Invitado
								- Admin, supervisor y editor

							Roles sean desacoplados (A || S || E)

							{
								"nombre":"Alex",
								"roles":["A", "S", "E"]
							}

							{
								"nombre":"Juan",
								"roles":["S", "E"]
							}

							{
								"nombre":"Luis",
								"roles":["A", "E"]
							}

							1. Obtener todos los usuarios que tienen rol: "Admin" (dentro del array de roles que tengan la letra "A")

							.find({roles:"A"})

							2. Obtener todos los usuarios que tengan el rol: "A" y "E".


							Operador OR:
								.find({$or: [{roles:"A"}, {roles:"E"}]})


							Si quiero "A" y "E" exclusivamente utilizo la siguiente query: 

								.find({roles: ["A","E"]})

							Obtener usuarios que contenga el rol "A" y "E"

								.find({roles: {$all: ["A", "E"]}})




