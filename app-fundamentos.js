const pi 	= 3.141516;

let nombre = "Alex" // Tipo String
let num    = 123
let num2   = 123.65
let verdadero = true // false
let arr 	= ["Manzana", "Naranja", "Pera"]
let persona = {
	nombre: "Alex",
	apellido: "Casadevall"
}

console.log("persona: ", persona) // Objeto
console.log("nombre: ", nombre)
console.log("numero: ", num)
console.log("numero 2: ", num2)
console.log("booleano: ", verdadero)
console.log("array: ", arr)
console.log("pi: ", pi)