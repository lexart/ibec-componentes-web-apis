// Requerir el modulo de ExpressJS en nuestro Backend
const express     = require('express');
const bodyParser  = require('body-parser');
const cors        = require('cors');
global.mongodb     = require('./services/mongodb.connect')

// Inicializar el express para comenzar a exponer nuestra API
const app = express();

const corsOptions   = {
    "origin": '*',
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    "preflightContinue": false,
    "allowedHeaders": ['Content-Type', 'Authorization'],
    "optionsSuccessStatus": 204
}

// ENABLE USES
app.use( cors(corsOptions) );

// Usar una APP externa a express a través del wrapper
app.use(bodyParser.json())

app.get('/', function (req, res) {
  res.set('Content-Type', 'application/json')
  let response = {texto: 'Hola mundo!'};
  

  // Resultado
  res.send(response);
});

// Lista de productos
app.get('/productos/all', function (req, res) {
  let Producto = require('./services/productos');

  let resData  = {error: "No existen productos"}
  
  // Modelo de datos
  Producto.obtenerProductos( function (response) {
      if(response.length > 0){
        resData = {
          response: response
        }
      }

      // Resultado, lo formatea en JSON
      res.set('Content-Type', 'application/json')
      res.send(resData);
  });

});


// Buscador de productos
app.post('/productos/find', function (req, res) {
  let Producto = require('./services/productos');
  // Esto es lo que me llega del postman
  let post       = req.body;

  let resData  = {error: "No existen productos"}

  // Acá llamo todos los productos
  let productos    = Producto.obtenerProductos();

  // Candidatos es el array resultado de a busqueda
  // Del obj que viaje por POST desde el postman
  // Y todos los productos que están en el modelo de "Producto"
  let candidatos   = Producto.buscarProductos(productos, post)

  // Pasa mano entre la variable resultante Candidatos y el reponse
  // Basicamente para seguir una lógica de respuesta
  let response = candidatos;

  if(response.length > 0){
    resData = {
      response: response
    }
  }

  if(post.cod == ""){
    resData  = {error: "Codigo no debe ser vacio"}
  }

  // Resultado, lo formatea en JSON
  res.set('Content-Type', 'application/json')
  res.send(resData);
});

app.post('/producto/new', function (req, res) {
  // Esto es lo que me llega del postman
  let post       = req.body;
  
  // Resultado, lo formatea en JSON
  res.set('Content-Type', 'application/json')

  // Ingreso documentos en la base de datos
  mongodb.insertProductos(db, [post], function (resData) {
    res.send(resData);
  })
})

app.put('/producto/edit/:id', function (req, res) {
  // Esto es lo que me llega del postman
  let post       = req.body;
  
  // Resultado, lo formatea en JSON
  res.set('Content-Type', 'application/json')

  // Actualizo documento en la base de datos
  mongodb.updateProductos(db, post, function (resData) {
    res.send(resData);
  })
})


// Obtener producto por ID
app.get('/productos/:id', function (req, res) {
  let Producto = require('./services/productos');
  // Obtengo el valor de :id de manera dinámica
  let id       = req.params.id;

  // Acá llamo todos los productos
  let productos = Producto.obtenerProductos();

  // Modelo de datos
  let response = Producto.obtenerProductoPorId(productos, id);

  // Resultado, lo formatea en JSON
  res.set('Content-Type', 'application/json')
  res.send(response);
});

// Levanta un webserver en el puerto 3000
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});