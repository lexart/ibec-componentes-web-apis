// Requerir el modulo de ExpressJS en nuestro Backend
const express     = require('express');
const bodyParser  = require('body-parser');
const cors        = require('cors');
global.mongodb     = require('./services/mdb.service')

// Inicializar el express para comenzar a exponer nuestra API
const app = express();

const corsOptions   = {
    "origin": '*',
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    "preflightContinue": false,
    "allowedHeaders": ['Content-Type', 'Authorization'],
    "optionsSuccessStatus": 204
}

// Usar una APP externa a express a través del wrapper
app.use(bodyParser.json())
// ENABLE USES
app.use( cors(corsOptions) );

app.get('/', function (req, res) {
  res.set('Content-Type', 'application/json')
  let response = {texto: 'Hola mundo!'};
  
  res.send(response);
});

// Obtener personas
app.get('/personas/all', function (req, res) {
  res.set('Content-Type', 'application/json')
  
  // Modularizar el servicio de personas
  const Persona = require('./services/personas')

  // Método para obtener todas las personas
  Persona.obtenerTodos( function (personas){
    res.send({response: personas})
  })
});

app.post('/persona/new', function (req, res) {
  res.set('Content-Type', 'application/json')
  let post = req.body;
  
  // Modularizar el servicio de personas
  const Persona = require('./services/personas')

  // Método para obtener todas las personas
  Persona.crear(post, function (response){
    res.send({response: response})
  })
});

app.put('/persona/edit/:id', function (req, res) {
  res.set('Content-Type', 'application/json')
  let post = req.body;
  let id   = req.params.id;

  // Modularizar el servicio de personas
  const Persona = require('./services/personas')

  // Método para obtener todas las personas
  Persona.actualizar(id, post, function (response){
    res.send({response: response})
  })
});

app.delete('/persona/delete/:id', function (req, res) {
  res.set('Content-Type', 'application/json')
  let id   = req.params.id;

  // Modularizar el servicio de personas
  const Persona = require('./services/personas')

  // Método para obtener todas las personas
  Persona.eliminar(id, function (response){
    res.send({response: response})
  })
});

// Levanta un webserver en el puerto 3000
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});