let Route = {
	changeState: function (state, callback){
		window.location.hash = '#!' + state;
		callback({nextState: state})
	}
}