app.factory('Persona', ['$http', function ($http){
	return {
		obtenerTodos: function (callback){
			$http.get(API + 'personas/all').then( function (res){
				let rs = res.data.response;
				callback(rs)
			})
		},
		guardar: function (id, obj, callback){
			if(id && id != ''){
				// Enviar por POST a la API
				$http.put(API + 'persona/edit/' + id, obj).then( function (res){
					let rs = res.data.response.result;
					callback(rs)
				})
			} else {
				$http.post(API + 'persona/new', obj).then( function (res){
					let rs = res.data.response.result;
					callback(rs)
				})
			}
		},
		eliminar: function (id, callback){
			$http.delete(API + 'persona/delete/'+id).then( function (res){
				let rs = res.data.response.result;
				callback(rs)
			})
		}
	}
}])