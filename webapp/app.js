
const API 	= 'http://localhost:3000/';

let app 	= angular.module('app', ['ngRoute'])

// Modulo de configuración
app.config(['$routeProvider', function($routeProvider) {
  $routeProvider
	  .when("/", {
	    templateUrl: "views/loginView.html",
	    controller: 'LoginCtrl'
	  })
	  .when("/dashboard", {
	    templateUrl: "views/dashboardView.html",
	    controller: 'DashboardCtrl'
	  })
	  .when("/personas", {
	    templateUrl: "views/personasView.html",
	    controller: 'PersonasCtrl'
	  })
	  .when("/personas/new", {
	    templateUrl: "views/personasFormView.html",
	    controller: 'PersonasCtrl'
	  })
	  .when("/personas/edit/:id", {
	    templateUrl: "views/personasFormView.html",
	    controller: 'PersonasCtrl'
	  })
}]);

// Crear el controller
app.controller('MainCtrl', ['$scope','$http','Persona', function($scope, $http, Persona){
	$scope.titulo = "Main ::"
}])