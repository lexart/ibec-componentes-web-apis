// Crear el controller
app.controller('PersonasCtrl', ['$scope','$http','Persona','$route', function($scope, $http, Persona, $route){

	console.log("PersonasCtrl :: ")

	$scope.titulo = "ABM Personas ::"

	$scope.q 	   = undefined
	$scope.persona = {}


	// Si tengo una ID de parametro de URL
	if($route.current.params.id){
		let id = $route.current.params.id;

		Persona.obtenerTodos( function (personas){
			$scope.personas = personas;

			$scope.personas.map( function (persona){
				if(persona._id == id){
					$scope.persona = persona;
					return;
				}
			})
		})
	}

	console.log("$route.current.params: ", $route.current.params)

	// Tiempo de ejecución
	$scope.personas = [];

	console.log("Persona: ", Persona)

	// LLamar personas desde un servicio
	Persona.obtenerTodos( function (personas){
		$scope.personas = personas;
	})

	// Tiempo de resolución del servidor
	// $http.get(API + 'personas/all').then( function (res){
	// 	let personas 	= res.data.response;
	// 	$scope.personas = personas;
	// })

	$scope.showTable 	 = true;
	$scope.toggleTable   = function (){
		$scope.showTable = !$scope.showTable;
	}

	$scope.goBack 		= function (){
		window.localStorage.setItem('usuarioEdit','')
		Route.changeState('personas', function (stateChanged){})
	}

	$scope.resetTable 	 = function (){
		$scope.showTable = true;
		$scope.persona   = {}

		// Obtener las personas siempre que se vaya a la tabla
		Persona.obtenerTodos( function (personas){
			$scope.personas = personas;
		})
		window.localStorage.setItem('usuarioEdit','')
	}

	$scope.editarPersona = function (persona){
		console.log("Cual persona? ", persona)
		$scope.persona = angular.copy(persona);

		Route.changeState('personas/edit/' + persona._id, function (stateChanged){})
	}

	$scope.guardarPersona = function (){
		var persona = {}

		let id = $scope.persona._id ? $scope.persona._id : undefined;

		persona = {
			nombre: $scope.persona.nombre,
			edad: $scope.persona.edad,
			documento: $scope.persona.documento
		}

		Persona.guardar( id, persona, function (result){
			if(result.ok == 1){
				alert("Persona creo/actualizó correctamente.")
				Route.changeState('personas', function (stateChanged){
					$scope.resetTable()
				})
			}
		})
	}

	$scope.eliminarPersona = function (persona){
		if(confirm("Esta seguro que desea eliminar esta persona?")){
			
			var id = persona._id; 

			Persona.eliminar(id, function (result){
				// Enviar por POST a la API
				if(result.ok == 1){
					alert("Persona se eliminó correctamente.")
					// Vuelvo a la tabla
					$scope.resetTable()
				}
			})
			
		} else {
			console.log("menos mal ...")
		}
	}
}])