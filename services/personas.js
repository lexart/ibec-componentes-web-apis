const modelo = 'personas'
let Persona = {
	obtenerTodos: function (callback){
		// Voy a llamar a la instancia de MongoDB
  		// Consultar por documentos de la colección "personas"
		mongodb.findDocuments(db, modelo, {}, function (response){
			// Cuando esté resuelto
			// Devolvé el array de elementos en la API
			callback(response);
		})
	},
	crear: function (persona, callback){
	  // Voy a llamar a la instancia de MongoDB
	  // Consultar por documentos de la colección "personas"
	  mongodb.insertDocument(db, modelo, persona, function (response){
	    
	    // Cuando esté resuelto
	    // Devolvé el array de elementos en la API
	    callback(response);
	  })
	},
	actualizar: function (id, persona, callback){
		  // Voy a llamar a la instancia de MongoDB
		  // Consultar por documentos de la colección "personas"
		  mongodb.updateDocument(db, modelo, id, persona, function (response){
		    
		    // Cuando esté resuelto
		    // Devolvé el array de elementos en la API
		    callback(response);
		  })
	},
	eliminar: function (id, callback){
	  // Voy a llamar a la instancia de MongoDB
	  // Consultar por documentos de la colección "personas"
	  mongodb.removeDocument(db, modelo, id, function (response){
	    
	    // Cuando esté resuelto
	    // Devolvé el array de elementos en la API
	    callback(response);
	  })
	}
}
module.exports = Persona;