// Agrego MongoDB
const MongoClient = require('mongodb').MongoClient;
const ObjectId    = require('mongodb').ObjectID;
const assert      = require('assert');


// Connection URL
const url = 'mongodb://localhost:27017';

// Base de datos
const dbName = 'abm_personas';

// Conectando a la base de datos
// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
  assert.equal(null, err);
  global.db = client.db(dbName);
  global.collection  = db.collection('productos');
});

const insertProductos = function(db, arr, callback) {

  // Insert some documents
  collection.insertMany(arr, function(err, result) {
    assert.equal(err, null);
    assert.equal(1, result.result.n);
    assert.equal(1, result.ops.length);
    console.log("Inserted 1 documents into the collection");
    callback(result);
  });
}

const findProductos = function(db, filter, callback) {

  // Find some documents
  collection.find(filter).toArray(function(err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs);
    callback(docs);
  });
}

const updateProductos = function(db, producto, callback) {

  // Update document where a is 2, set b equal to 1
  console.log("producto db ::", producto)
  collection.updateOne(
  	{_id: ObjectId(producto._id)}, { $set: { 
    			nombre: producto.nombre,
    			precio: producto.precio,
    			cantidad: producto.cantidad
    		} 
    }, function(err, result) {
    	console.log("err: ", err)
    	console.log("result: ", err)
    assert.equal(err, null);
    assert.equal(1, result.result.n);
    console.log("Updated the document with the field a equal to 2");
    callback(result);
  });
}

module.exports = {
	insertProductos: insertProductos,
	findProductos: findProductos,
	updateProductos: updateProductos
}