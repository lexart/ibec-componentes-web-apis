let Producto = {
	obtenerProductos: function (cb){
		mongodb.findProductos(db, {}, function (arr){
			cb(arr)
		})
	},
	buscarProductos: function (arr, obj){
		let candidatos = []

		// Recorro un array (quiero creer que es de productos)
		for (let i = arr.length - 1; i >= 0; i--) {
			let producto = arr[i];
			// Encuentro un producto que tiene el cod contenido del obj.cod
			// Obj.cod ~ {cod: "AB"}
			if(producto.cod.includes(obj.cod) || producto.nombre.includes(obj.nombre)){
				// Si encuentro agrego el candidato al array
				candidatos.push(producto)
			}
		}

		// Devuelvo el array de candidatos
		return candidatos;
	},
	obtenerProductoPorId: function (arr, id){
		let producto = {}

		// Recorro un array (de productos)
		for (var i = arr.length - 1; i >= 0; i--) {
			let prod = arr[i]

			// Verifico si prod.id es igual a la id dinámica
			if(prod.id == id){
				producto = prod;
				// Una vez que encuentra no es necesario seguir recorriendo
				break;
			}
		}

		// Retornar el producto
		return producto;
	}
}
module.exports = Producto;