const API = 'http://localhost:3000/agenda/'

// Inicializo la APP de angularJS
let app = angular.module('app',['ngRoute'])

// Inicializar la config del NgRouter

// Modulo de configuración
app.config(['$routeProvider', function($routeProvider) {
  $routeProvider
	  .when("/", {
	    templateUrl: "views/indexView.html",
	    controller: 'AgendaCtrl'
	  })
	  .when("/contacto/new", {
	    templateUrl: "views/formView.html",
	    controller: 'AgendaCtrl'
	  })
	  .when("/contacto/edit/:id", {
	    templateUrl: "views/formView.html",
	    controller: 'AgendaCtrl'
	  })
}]);