app.factory('ContactoService', ['$http', function ($http){
	let modelo = ''
	return {
		obtenerTodos: function (callback){
			modelo = 'contactos/all'
			
			$http.get(API + modelo).then( function (res){
				let response = res.data.response;
				callback(response)
			})
		},
		obtenerPorId: function (id, callback){
			modelo = 'contactos/' + id
			
			$http.get(API + modelo).then( function (res){
				let response = res.data.response;
				callback(response)
			}, function (fail){
				callback({error: "Error de servidor"})
			})
		},
		guardar: function (contacto, callback){
			if(contacto._id){
				
				modelo = 'contacto/edit/' + contacto._id

				// Editar
				$http.put(API + modelo, contacto).then( function (res){
					let response = res.data.response;
					callback(response)
				})
			} else {
				modelo = 'contacto/new'

				// Crear
				$http.post(API + modelo, contacto).then( function (res){
					let response = res.data.response;
					callback(response)
				})
			}
		}
	}
}])