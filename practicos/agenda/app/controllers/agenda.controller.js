app.controller('AgendaCtrl', ['$scope','ContactoService','$route', function ($scope, ContactoService, $route){
	console.log("Agenda Ctrl ::")
	$scope.titulo = "Agenda";
	$scope.contactos = []
	$scope.contacto  = {}

	let id = $route.current.params.id;

	if(id){
		// Integración de la API de contactos
		ContactoService.obtenerPorId(id, function (contacto){
			if(!contacto.error){
				$scope.contacto  = contacto;
			} else {
				alert(contacto.error)
				window.location.hash = '#!/'
			}
		})
	} else {

		// Integración de la API de contactos
		ContactoService.obtenerTodos( function (contactos){
			$scope.contactos = contactos;
		})
	}

	$scope.guardar = function (){
		console.log("$scope.contacto: ", $scope.contacto)

		// Servicio para guardar
		ContactoService.guardar( $scope.contacto, function (resultado){
			console.log("resultado ::", resultado)
			if(resultado.result.ok){
				// Estado inicial
				window.location.hash = '#!/'
			}
		})
	}

}])