// Requerir el modulo de ExpressJS en nuestro Backend
const express     = require('express');
const bodyParser  = require('body-parser');
const cors        = require('cors');
global.mongodb    = require('./services/mdb.service')

// Inicializar el express para comenzar a exponer nuestra API
const app = express();

const corsOptions   = {
    "origin": '*',
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
    "preflightContinue": false,
    "allowedHeaders": ['Content-Type', 'Authorization'],
    "optionsSuccessStatus": 204
}

// Usar una APP externa a express a través del wrapper
app.use(bodyParser.json())
// ENABLE USES
app.use( cors(corsOptions) );

app.get('/', function (req, res) {
  res.set('Content-Type', 'application/json')
  let response = {texto: 'Hola mundo!'};
  
  res.send(response);
});

// Obtener personas
app.get('/agenda/contactos/all', function (req, res) {
  res.set('Content-Type', 'application/json')
  
  // // Modularizar el servicio de personas
  const Contacto = require('./services/contacto.service')

  // // Método para obtener todas las personas
  Contacto.obtenerTodos( function (personas){
    res.send({response: personas})
  })
});

app.get('/agenda/contactos/:id', function (req, res) {
  res.set('Content-Type', 'application/json')

  let id = req.params.id;
  
  // // Modularizar el servicio de personas
  const Contacto = require('./services/contacto.service')

  // // Método para obtener todas las personas
  Contacto.obtenerPorId(id, function (personas){
    res.send({response: personas[0]})
  })
});

// Obtener personas
app.post('/agenda/contacto/new', function (req, res) {
  res.set('Content-Type', 'application/json')
  
  let contacto  = req.body;

  // // Modularizar el servicio de personas
  const Contacto = require('./services/contacto.service')

  // // Método para obtener todas las personas
  Contacto.crear( contacto, function (personas){
    res.send({response: personas})
  })
});

app.put('/agenda/contacto/edit/:id', function (req, res) {
  res.set('Content-Type', 'application/json')
  
  let contacto  = req.body;
  let id        = req.params.id;

  // // Modularizar el servicio de personas
  const Contacto = require('./services/contacto.service')

  // // Método para obtener todas las personas
  Contacto.actualizar(id, contacto, function (personas){
    res.send({response: personas})
  })
});


// Levanta un webserver en el puerto 3000
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});